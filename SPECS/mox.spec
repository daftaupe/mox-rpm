%define debug_package %{nil}
%define repo github.com/mjl-/mox
Name:           mox
Version:        0.0.12
Release:        1%{?dist}
Summary:        Modern full-featured open source secure mail server for low-maintenance self-hosted email

License:        MIT
URL:            https://%{repo}
Source0:        https://%{repo}/archive/refs/tags/v%{version}.tar.gz

BuildRequires:  git golang npm systemd

AutoReq:        no
AutoReqProv:    no

%description
Complete email solution
For sending and receiving email. With support for IMAP4, SMTP, SPF, DKIM, DMARC, MTA-STS, DANE and DNSSEC, reputation-based and content-based junk filtering, Internationalization (IDNA), automatic TLS with ACME and Let's Encrypt, account autoconfiguration, webmail.
Quick & easy
Use the quickstart command to set up mox for your domain(s) within 10 minutes. You'll get a secure mail server with a modern protocol stack. Upgrades are mostly a matter of downloading the new version and restarting. Maintenance via web interface (easy) or config file (powerful). No dependencies.
High quality and secure
Mox has a modern Go code base with plenty of automated tests, automated integration tests, is manually tested against popular mail server and client software, and is fuzz-tested. The code is well-documented and cross-referenced with the relevant standards (RFC's).

%prep
mkdir -p %{_builddir}/src/github.com/mjl-
cd %{_builddir}/src/github.com/mjl-
tar -xvzf %{_sourcedir}/v%{version}.tar.gz
mv mox-%{version} mox
cd mox

%build
export GOPATH="$(pwd)"
export PATH=$PATH:"$(pwd)"/bin
cd src/%{repo}
make build
make install

%install
mkdir -p %{buildroot}%{_bindir}
cp src/%{repo}/%{name} %{buildroot}%{_bindir}


%files
%{_bindir}/%{name}
%license src/%{repo}/LICENSE.MIT
%config(noreplace) %{_sysconfdir}/%{name}/mox.conf


%changelog
* Mon Nov 25 2024 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.0.12-1
- Update to version 0.0.12

* Wed Mar 13 2024 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.0.10-1
- Update to version 0.0.10

* Mon Mar 11 2024 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.0.9-1
- Initial release for version 0.0.9
